# Monero Adoption Animated Videos

## Name
Monero Adoption Animated Videos 2023

## Description
This is the development of the project initially proposed for the associated following [Monero CCS request](https://repo.getmonero.org/monero-project/ccs-proposals/-/blob/master/savandra-videos-for-monero.md)

## Support
Contact u/dsmlegend or u/savandra on Reddit, or @dsmlegend:matrix.org or @savandra:matrix.org on Matrix.

## Roadmap
Five videos are planned. The intended workflow is that each will be completed in full before work on the next starts.

## Contributing
Any contributions to video scripting by members of the Monero community is welcome. Submit a pull request to the appropriate script doc. Please also visit the dedicated matrix channel for live discussion: [Monero Animated Videos 2023](https://matrix.to/#/#Monero-Animated-Vids-2023:matrix.org)

## Authors and acknowledgment
Project conceived of and driven by video animator savandra. See previous work at this [youtube channel](https://www.youtube.com/channel/UCnjUpT9gGxyQ_lud7uKoTCg).

## License
MIT

## Project status
Video 1 is nearing completion, with final touchups of voice/video synchronisation. Currently reviewing community suggestions for content of [video 2 script](Video 2 Script: What is a Monero Wallet?.md), after which a revised draft will be synthesised.

## Note on the Bonus Video
This video was not covered under the original CCS proposal, so it will only be made if enough resources remain after completing the main 5 videos, or otherwise if additional funds are raised for it specifically.

## XMR Address
For dsmlegend: **8BXPxEYVtFAZSKJnvyrRYm8GeE9VBnzK4fhY2WtimUksHNXgmSKHfR9CrACRjqh1MgJ961soE2uk1AR9b2px3F4AToW1TXw**