# Video 5: What to do with Monero revenue?
## Key Points
1. Network value lies in providing common standard for participants, tokens can function as currency

2. Network broadened lowers financial friction, therefore

3. Best to spend tokens on wages/materials/bills

4. More participation improves economic freedom, independence from banks, inflation resistance and security/confidentiality

5. Benefit of Monero as transferrable liquid asset, over fiat

6. Potential appreciation of token value in near term as greater demand meets steady supply

7. If not directly spendable, Monero can be sold at market for fiat

8. Digital currency exchange companies offer direct fiat sales in some jurisdictions (Binance, Kraken, DV Chain, Bitfinex)

9. Open marketplace escrow platform (Localmonero). Explain make or take offer.

9. Via intermediary token (bitcoin)

## Draft
Welcome to part 5 of this educational animated video series, this time on what a business can do with the Monero it has obtained from payments for goods and services. Today, we're going to be discussing some of the options and considerations for managing and using your Monero funds.

First, let's briefly review the benefits of using Monero for your business. Monero is a decentralized digital currency that allows for fast, secure, and private transactions. By accepting Monero payments for your goods and services, you can provide your customers with an additional payment option and take advantage of the features and benefits of Monero.

Once you have received Monero payments for your goods and services, you'll need to decide what to do with your Monero funds. There are several options available, and the best one for you will depend on your individual business needs and goals.

The first and simplest option is to hold on to your Monero funds and use them for future outgoing transactions. This can be a good choice if you expect to cover future expenses using Monero, or if you simply want to use Monero as a form of savings or an insurance of last-resort that cannot be frozen or stolen. Monero is a highly liquid and widely accepted digital currency, so you can also easily convert it into other cryptocurrencies or fiat currencies when needed.

This brings us to the second option, which is to convert your Monero funds into your local fiat currency. This can be a good choice if you need to pay bills or expenses that are denominated in your national currency, or if you want to withdraw your Monero earnings as a salary and use them for personal expenses. You can use a Monero exchange service to convert your Monero funds into your local fiat currency, and then withdraw the funds to your bank account or use them for other purposes.

A third and most beneficial option is to use your Monero funds directly to make purchases or cover expenses. For example, you can use your Monero funds to buy goods and services from other Monero merchants, or you can use it to pay staff salaries or bonusses. Paying suppliers or staff in Monero is a good way to remain within the Monero economy and aid in its establishment in your community, while also avoiding the financial friction of conversions. Such organic circulation amplifies the value of Monero as a non-corporate and independent tool for commercial activities and allows businesses to be more free from undue corporate or government control.

In conclusion, there are several options for using the Monero that you have obtained from payments for your goods and services. Whether you choose to hold onto your Monero funds, convert them into fiat currency, or use them for purchases or wages, you can manage your Monero finances in a way that meets your business needs and goals. Thank you for watching this educational animated video on what your business can do with the Monero it has obtained from payments for goods and services.
