# Video 2: What is a Monero Wallet?
## Key Points
### What is a wallet?
1. A set of accounts controlled by the same individual/entity
2. Consists of a set of master digital keys, akin to passwords
    * Spending key: to authorise outgoing transactions
    * Viewing key: to identify incoming transactions
3. The keys are plugged into software that communicates with the global Monero network.
4. These programs or apps are also referred to as ‘wallets’, even though they are technically wallet handling software.
5. Uses the spend key to authorise (sign) transactions. Broadcasts this to a network node of your choice. Node validates that the tokens were spendable (received into the wallet earlier and belongs to the wallet).
6. Uses view keys to monitor the network for transactions designated to the wallet. Identifies tokens authorised by a payer to be transferred to your wallet.
7. The network consists essentially of a ledger that keeps track of how many tokens are allocated to each wallet. Tokens are only reallocated when the wallet keys authorises such transactions using the digital keys that they consist of (which are like spending passwords)
8. These digital keys are, simply put, long unique codes. Due to the difficulty of copying and typing these manually, a ‘mnemonic seed’ algorithm is usually used to repackage these codes into a set of 25 simple words that are easy to record and retype.
9. This means that access to and authority to spend the funds associated with a wallet can be carried entirely just with these 25 words. All wallet software packages can interpret this format to instantly unpack the digital keys on any device, anywhere in the world.
### Creating a new wallet
1. Technically, the wallet keys could just be calculated manually. However, due to the special relationship between the spend and view keys and the particular format recognised by the network, this is probably impractical.
2. The wallet software solutions that allow control of funds are also conveniently able to generate new wallet keys.
3. Alternatively, one may purchase hardware devices that are entirely dedicated to performing this in the most secure way possible.
4. These are small devices (like USB sticks) that have no internet connectivity, in order to be hack-proof. They generate and store your wallet keys.
5. They connect only to your wallet software on your own internet-connected device and never to the broader web.
    * Examples are Ledger Nano S/X or Trezor T.
6. Wallet handling software is available on 
    * Desktop: Monero GUI, Featherwallet, MyMonero
    * iOS: Cake Wallet, MyMonero, Monero.com
    * Android: Cake Wallet, Monerujo, MyMonero, Monero.com
7. Once downloaded, the software will run you through the steps necessary to create the wallet and store a physical backup (recall mnemonic seed)

## Draft
Hello and welcome to part 2 of this educational animated video series on Monero. Today, we're going to be discussing what a Monero wallet is, how it works, and how to set one up.

First, let's define what a wallet is. In the context of Monero, a wallet is software that allows you to securely store, send, and receive Monero. Your Monero wallet holds your private keys, which function as your network password.

Imagine your Monero wallet as a kind of password manager that you use to access your Monero funds. Just like a password, your private keys are unique to you and should be kept secret at all times. Your wallet software uses these private keys to sign off on outgoing transactions and to discover incoming transactions on the Monero network.

The relationship between the Monero transaction database, your wallet, and your private keys is important to understand. The Monero transaction database, or ledger, is a shared, decentralized record of all Monero transactions. Your wallet is the interface that you use to interact with the Monero ledger, and your private keys are what allow you to authorize spending and decrypt incoming transactions. Decryption is necessary, because transaction data is protected by strong encryption exactly because it is not hidden behind a central authority, but a matter of public record. Your wallet is the tool that allows you to access and manage your personal accounts on the network, which is inaccessible to anyone else.

In order to make it easier to manage your private keys, your Monero wallet software allows you to package them into mnemonic seeds. These are a series of words that you can use to regenerate your private keys in case you lose access to your wallet. It's important to keep your mnemonic seed safe and secure, as it is the only way to recover your Monero funds in case of device loss or theft.

Now that we've discussed what a Monero wallet is and how it works, let's talk about how to set one up. There are a few different ways to create a Monero wallet, but the most common method is to use wallet software to generate fresh private keys. This can be done using a variety of tools and options, depending on the wallet software that you choose.

Another option for setting up a Monero wallet is to use a purpose-built hardware device, such as a Ledger or Trezor. These devices allow you to securely generate and store your private keys, and they have the added advantage of being portable and extremely resistant to hacking.

There are several good wallet software options available on different platforms, each with its own pros and cons. Some popular options include Monero GUI and Featherwallet for desktop, and Monero.com or Monerujo for mobile. It's important to choose a wallet software that meets your needs and that you feel comfortable using.

Once you have chosen your wallet software, you will need to configure it according to your preferences. This typically involves setting up a spend wallet and a view-only wallet, each with its own set of private keys. The view-only wallet allows you to view your Monero balance and transaction history, while the spend wallet allows you to authorize outgoing transactions in addition to viewing them. You may prefer to keep just a view-only wallet on premises for a physical store, since funds cannot be stolen from it, even under threat.

In conclusion, a Monero wallet is a piece of software that allows you to securely store, send, and receive Monero. Your wallet holds your private keys, which are used to authorize spending and discover incoming transactions on the Monero network. There are several good wallet software options available, and you can choose to set up your wallet using software on an exiting device such as a laptop, tablet or phone, or a on a purpose-built hardware device. Once your wallet is set up, you can configure it according to your preferences and start using it to manage your Monero funds. Thank you for watching this educational video on Monero wallets and stay tuned for the next video where we will explain how to receive Monero payments.

# Notes in Description
## Resources
### Wallet Software
1. https://www.getmonero.org/downloads/
2. https://featherwallet.org/
3. https://www.monerujo.io/
4. https://monero.com/wallets

### Wallet Hardware Devices
1. https://trezor.io/
2. https://www.ledger.com/

### Mnemonic Seed Explainations
1. https://www.getmonero.org/resources/moneropedia/mnemonicseed.html
2. https://www.getmonero.org/resources/user-guides/restore_account.html
3. https://scribe.rip/-seed-with-offset-passphrase-works-in-monerujo-416ff5198b2e

## Community Discussion
1. Reddit: r/Monero
2. Twitter: Look for accounts tagged 'Monero' or 'XMR', e.g. @Monero, @MoneroTalk
