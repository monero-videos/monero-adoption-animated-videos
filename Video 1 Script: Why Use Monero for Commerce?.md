# Video 1: Why Use Monero for Commerce?

Discover Monero with this condensed video series and learn about its features, benefits, and practical use as a merchant **[show here a series of small frames that represent each of the other videos 2-5, with their titles]**. Funded by anonymous Monero users who have experienced its advantages, this series aims to educate and inspire businesses to adopt Monero for secure and private transactions, free from middleman interference and hazards such as spying, freezing of funds, chargebacks, and deplatforming. 

**[What is Monero?]**

Monero, an open-source payment system, operates as a decentralized network with equal peers enforcing rules through automation, making it accessible and easy to use, even with a mobile app.

Monero offers advantages over traditional payment systems like PayPal, Visa, and AMEX with no background checks, credit evaluations, sign-up fees, subscription fees, or registration processes. Monero's low, flat fees **[asterisk with footnote: exact fee will vary depending on network congestion and other factors]** are paid by the sender and go back to its community of network contributors **[show here a table of comparison between fees. Paypal: 2.6% + 30c; Visa/MasterCard: 1.5%-2.5%; AMEX: 2.5%-3.5%; Monero: <1c. Especially finding a way to show that percentage fees imply they can see your amounts, whereas the Monero network can only charge a flat fee because the amount is confidential]**. With sophisticated encryption and verification methods that prioritize privacy **[a simple animation that illustrates that transactions cannot be traced backwards]**, Monero protects users from digital surveillance while optionally providing transparent, publicly auditable receipts for accountability **[show here an animation that clearly illustrates that the transactor issues the receipt (tx proof) deliberately]**, making dispute resolution possible without a central authority.

Operating on its own currency, XMR, Monero offers unrestricted international use and independence from central banks. XMR exists solely in a virtual form, monitored by network peers tracking the total supply **[show here a representation of the peers as ordinary desktops/laptops to illustrate that it is a grassroots system and doesn't require owning a supercomputer, and how they check each other's work]**, and its decentralized structure makes it resistant to censorship and manipulation with transparent, volunteer-driven development.

**[Why use Monero for commerce?]**

Monero offers the benefits of electronic payments, eliminating the need for cash counting or safes, while retaining the advantage of cash where there is no central point of failure and hence no potential downtime **[a graphic that illustrates the robustness of a distributed network that keeps operating even if a few nodes go offline would be good. Can also contrast with a centralised service going offline and bringing everything to a halt with it]**. Free for business and inexpensive for customers, Monero works globally without restrictions, and transactions are irreversibly settled after a few minutes with only recipients able to issue a refund. 

Monero is popular among younger generations, those seeking financial independence, and international travelers. Use Monero to satisfy this market segment to both expand your customer base and increase sales.

Explore the resources below or watch these videos to learn about Monero's technical details **[display other channel videos on screen]**. In our next video, we will guide you through setting up a free Monero wallet in just a few minutes.


# Notes in Description
## Resources
1. 'What is Monero' playlist: https://www.youtube.com/watch?v=TZi9xx6aiuY&list=PLV_giHgwBqwzo3UXbDbOdByt4k4GjcbnX
2. Monero website: https://www.getmonero.org
3. Advanced user guides: https://moneroguides.org/

## Explainer Videos
1. How transactions are transmitted, recorded, and the network maintained: https://www.youtube.com/watch?v=6DQb0cMvU7I
2. How the destinations of transactions are encrypted, to protect recipient privacy: https://www.youtube.com/watch?v=6DQb0cMvU7I
3. How the origin of transactions are kept confidential, to protect sender privacy: https://www.youtube.com/watch?v=zHN_B_H_fCs
4. How transaction amounts are kept confidential yet publicly auditable: https://www.youtube.com/watch?v=M3AHp9KgTkQ
5. How the online identity of senders is protected when using Monero: https://www.youtube.com/watch?v=bxQ0ic1Fccs

## FAQs
1. How much are Monero transaction fees? https://www.monero.how/monero-transaction-fees 
2. Where can I find the open-source software repository? https://github.com/monero-project
3. Can I list my business on a directory? 
    * https://www.acceptedhere.io/catalog/currency/xmr/
    * https://cryptwerk.com/pay-with/xmr/
    * https://mycompass.ie/monero/
    * https://xmr.directory/
    * https://monerica.com/

## Community Discussion and Help
1. Reddit: r/Monero
2. Twitter: Look for accounts tagged 'Monero' or 'XMR', e.g. @Monero, @MoneroTalk
3. Matrix IM: https://matrix.to/#/#monero-support:monero.social

Video production was funded by generous anonymous donors through the Monero Community Crowdsourcing System: https://ccs.getmonero.org/proposals/savandra-videos-for-monero.html