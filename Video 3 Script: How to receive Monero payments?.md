# Video 3: How to receive Monero payments?
## Key Points
1. Explain accounts within wallets

2. Explain how subaddresses serve as payment/customer identifiers

3. Explain how invoice is generated as QR, or for manual entry (subaddress plus amount)

4. Explain how market value of Monero fluctuates and the option for calculating amount with reference to local fiat

5. Wallet software discovers incoming txn in seconds

6. Explain block confirmations and risk of 0-conf txns for irreversible purchases

7. List software options suitable for PoS setup and briefly list pros/cons of each

8. Visual walkthrough of generating QR code and receiving txn for one or two selected options (e.g. Bitrequest, Feather)

9. List options for web store

10. Visual walkthrough of one option (e.g. Woocommerce plugin)

11. Visual walkthrough of manual invoicing

12. Mention 3rd party solutions that allow auto-conversion to fiat (e.g. Nowpayments)

## Draft
Welcome to part 3 of educational animated video series on accepting Monero payments for your business. Today, we're going to be discussing the technical details of how to set up Monero payment processing for your goods and services.

In order to accept Monero payments, you'll need a Monero wallet that is capable of receiving Monero funds. We discussed this process in the previous video, so go check it out if you haven't already! 

Monero wallets have support for multiple accounts, which allow you to organize your funds and keep track of your transactions. Another useful feature of Monero wallets is the ability to generate subaddresses. These are additional payment addresses that you can use to identify specific customers or transactions. For example, you can generate a unique subaddress for each of your customers, and use it as a payment identifier when they make a purchase.

In order to receive a Monero payment, you'll need to generate an invoice. This can be done using your Monero wallet software, and it typically involves generating something as simple as a QR code for scanning or a string of text for manual copy-and-paste entry. The invoice will minimally contain your payment address (or subaddress) and the amount of Monero that the customer needs to send. A single catch-all static address with no amount specified is also suitable for accepting donations, if required.

When relying on Monero for payments, it's important to note that the market value of Monero can fluctuate, so you may want to include a reference to your local fiat currency in the invoice. This will allow the customer to see the equivalent amount in the dominant national currency where they live, and it will help prevent confusion or disputes over the expected payment amount. Most software solutions include such currency reference functionality.

Once a customer has sent a Monero payment to your payment address (or subaddress), your Monero wallet will automatically discover the incoming transaction and show it in your transaction history. This typically happens within a few seconds, so you can be confident that the payment has been received and is being processed on the Monero network.

However, it's important to note that Monero transactions require discrete batch confirmations in order to be considered finalised. This means that there is a small risk of a transaction being reversed if it has not yet received one or more network confirmations. For this reason, it's generally not recommended to accept 0-confirmation transactions for large, irreversible purchases. Formal network confirmations are processed every 2 minutes on average, with some variability.

Now that we've discussed the basics of accepting Monero payments, let's talk about some software options that are suitable for setting up a point-of-sale system for your business. Some popular options include Bitrequest, Hotshop, Feather, and Monerujo. Each of these options has its own pros and cons, so it's important to choose the one that meets your business needs and that you feel comfortable using.

In addition to point-of-sale systems, there are also several options for accepting Monero payments on your web store. For example, if you use a popular e-commerce platform like WooCommerce, you can install a Monero plugin that will allow you to accept Monero payments directly on your website. Such plugins exist for most notable e-commerce platforms, so take a look at the links in the description.

Alternatively, you can use a manual invoicing system to receive Monero payments. This involves generating an invoice as we discussed earlier, and then sending it to the customer via email or another communication channel. The customer can then use their Monero wallet to send the payment to your payment address (or subaddress) as specified in the invoice, without requiring any further action on your part. This is a simple option that might be suitable for someone who performs low-volume or customised services that don't require instant payments or automated systems.

One final option to consider is using a 3rd party solution that allows for automatic conversion of Monero payments into your local currency. For example, Nowpayments is a service that allows you to receive Monero payments and automatically convert them into your local fiat currency. This can save you the time and effort of using a Monero exchange service to manually convert your Monero funds, if indeed you have the need to do so.

In conclusion, there are several options available for accepting Monero payments for your business. Whether you use a point-of-sale system, a web store, or a manual invoicing system, you can easily set up Monero payment processing and start accepting Monero payments from your customers. Thank you for watching this educational animated video on accepting Monero payments for your business, and stay tuned for the next video on how to keep track of your Monero cashflow.

# Notes in Description
## Resources
### Wallet Software
1. https://www.getmonero.org/downloads/
2. https://featherwallet.org/
3. https://www.monerujo.io/
4. https://monero.com/wallets

### Account and Subaddress Explanations
1. https://monerodocs.org/public-address/subaddress/
2. https://anhdres.medium.com/how-moneros-accounts-and-subaddresses-work-in-monerujo-4fa7df0a58e4

### Physical POS
1. https://www.bitrequest.io/
2. https://github.com/CryptoGrampy/HotShop#hotshop
3. https://www.getmonero.org/downloads/
4. https://featherwallet.org/
5. https://www.monerujo.io/

### Online commerce
1. https://github.com/monero-integrations/monerowp
2. https://sethforprivacy.com/guides/accepting-monero-via-btcpay-server/

### 3rd-Party Solutions
1. https://nowpayments.io/supported-coins/monero-payments

## Community Discussion
1. Reddit: r/Monero
2. Twitter: Look for accounts tagged 'Monero' or 'XMR', e.g. @Monero, @MoneroTalk
