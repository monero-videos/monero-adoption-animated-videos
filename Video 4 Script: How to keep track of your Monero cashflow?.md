# Video 4: How to keep track of your Monero cashflow?
## Key Points
1. Monero payments can be recorded in the same manner as cash payments

2. For tax, local fiat value must be recorded

3. Overview of most common tax implications (CGT upon disposal)

4. Usually no need if utilising 3rd party payment gateway for instant auto-conversion

5. If taking direct payments, wallet software keeps local data

6. Explain what data is stored on ledger database vs must be saved locally

7. Show examples of transaction records (GUI, Feather)

8. Explain utility of various txn proofs and demonstrate how they can be performed

## Draft
Welcome to part 4 of this educational animated video series on keeping track of your Monero cashflow for your business. Today, we're going to be discussing how to manage and monitor your Monero transactions in order to better understand and control your Monero cashflow.

First, let's talk about the importance of keeping track of your Monero cashflow. As a business owner, you want to make sure that you are able to maintain a clear and accurate picture of your Monero finances. This will help you avoid potential problems or misunderstandings, particularly when the management of finances are shared among two or more individuals.

Recall from the first video that one of the key benefits of using Monero for your business is that it provides a verifiable and unchangeable record of all transactions on the Monero distributed database. This means that you can easily track and monitor your Monero transactions from anywhere in the world, and you can have confidence in the accuracy and security of your financial data particularly when you maintain your own local copy of the transaction database. This can be done from any device that has access to the internet, at any time, using only the wallet keys that were introduced in video 2 of this series. Refer to our technical explanatory video series, linked below, for an explanation on why the network is more robust than centralised payment solutions that may go offline or otherwise become inaccessible.

When accepting payments, it's worth mentioning that Monero payments can be recorded in the same manner as cash payments, and they can be treated as such for tax purposes. This means that you might need to record the local fiat value of each Monero payment that you receive, in order to comply with your tax obligations that would typically be denominated in your national currency.

One common tax implication of using Monero for your business is the potential for capital gains tax (CGT) upon disposal of your Monero funds, if the value has risen substantially since you received it. This means that if you sell your Monero for a profit, you may be required to pay tax on the gain. However, there is no need for this if you make use of an auto-conversion service, as introduced in video 3 of this series.

In addition to tracking and monitoring your Monero transactions, it's also important to understand the utility of various transaction proofs and how they can be performed.

Transaction proofs are a way of proving that a Monero transaction occurred on the Monero network. They can be useful in various situations, such as resolving disputes or verifying the authorship of a transaction.

There are several types of transaction proofs that can be performed. For example, you can prove that a transaction occurred, prove that you were the author of a transaction, or simply prove that you are the owner of a certain payment address. 

To perform a transaction proof, you'll need to use a Monero wallet that supports this feature. Most Monero wallets for desktops have this capability, and they typically provide instructions on how to generate and verify transaction proofs. Even if the wallet is mainly operated from a mobile device, it can also be accessed on desktop as needed to generate proofs, should the need arise.

In conclusion, keeping track of your Monero cashflow is an important part of managing your Monero finances for your business. By using a Monero wallet, you can easily track and monitor your Monero transactions and always have a clear and accurate picture of your Monero finances at all times. Understanding the utility of various transaction proofs and how to perform them can also be useful in certain situations. Thank you for watching this educational animated video on keeping track of your Monero cashflow for your business, and stay tuned for the next video on what to do with your Monero revenue.
